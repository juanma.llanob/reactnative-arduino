import React, { Component } from "react";
import {StyleSheet} from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Item, Input, Label } from "native-base";



 class Login extends Component {
  register =() =>{
    this.props.navigation.navigate('Register')
  }
  menu =() =>{
    this.props.navigation.navigate('Menu')
  }
  render() {
    return (
      
      <Container  >
        <Content padder contentContainerStyle={styles.contenido} >
          <Card>
            <CardItem header >
              <Text style={styles.tittle} >Login </Text>
            </CardItem>
            <CardItem >
              <Body>
                <Item floatingLabel style={styles.input}>
                  <Label>Username</Label>
                  <Input />
                </Item>
                <Item floatingLabel style={styles.input}>
                  <Label>Password</Label>
                  <Input />
                </Item>
              </Body>
            </CardItem>
            <CardItem  >
            <Button primary style={styles.btn_primary} onPress={this.menu}><Text> INGRESAR </Text></Button>

            </CardItem>
            <CardItem>
            <Button primary style={styles.btn_secundary} onPress={this.register}><Text> REGISTRAR </Text></Button>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

import fondo from '../../../assets/fondo.jpg'
const styles = StyleSheet.create({
 
  contenido: {
    flex : 1,
    justifyContent : 'center',
  },
  tittle : {
    fontSize : 30,
    color : 'green',
    textAlign : 'center',
    width : '100%'
  },
  btn_primary : {
    flex:1,
    backgroundColor: 'green',
    textAlign: 'center',
    justifyContent : 'center',
    width : '100%'
  },
  btn_secundary : {
    flex :1,
    backgroundColor: 'silver',
    textAlign: 'center',
    justifyContent : 'center',
    width : '100%'
  },
  input:{
    marginTop : 20
  }
})


export default Login;