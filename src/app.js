import React  from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import RegisterScreen from './modules/login/register';
import LoginScreen from './modules/login/login';
import MenuScreen from './modules/menu';
import HumedadScreen from './modules/Humedad/humedad';
import TemperaturaScreen from './modules/temperatura/temperatura';
import RedScreen from './modules/usuario/usuario';
import UsuarioScreen from './modules/red/red';



const LoginNavigation = createStackNavigator({
  Login : {
    screen : LoginScreen,
    navigationOptions :{
      title : 'GVAPP'
    }
  },
  Register : {
    screen : RegisterScreen,
    navigationOptions:{
      title : 'REGISTRO'
    }
  },
  Menu : {
    screen : MenuScreen,
    navigationOptions :{
      title : 'MENU'
    }
  },
  Humedad : {
    screen : HumedadScreen,
    navigationOptions : {
      title : 'HUMEDAD'
    }
  },
  Temperatura : {
    screen : TemperaturaScreen,
    navigationOptions : {
      title : 'TEMPERATURA'
    }
  },
  Red : {
    screen : RedScreen,
    navigationOptions : {
      title : 'RED'
    }
  },
  Usuario : {
    screen : UsuarioScreen,
    navigationOptions : {
      title : 'USUARIO'
    }
  },
},{headerLoyoutPreset : 'center'});


export default createAppContainer(LoginNavigation);