import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';


export default class Menu extends Component {
    menu =() =>{
        this.props.navigation.navigate('Menu')
      }
      humedad =() =>{
        this.props.navigation.navigate('Humedad')
      }
      temperatura =() =>{
        this.props.navigation.navigate('Temperatura')
      }
       red=() =>{
        this.props.navigation.navigate('Red')
      }
      usuario=() =>{
        this.props.navigation.navigate('Usuario')
      }

      
  render() {
    return (
      <Container>
        <Content />
        <Footer>
          <FooterTab>
            <Button badge vertical onPress={this.humedad}>
              <Badge><Text>2</Text></Badge>
              <Icon name="cloud" />
              <Text>Hume</Text>
            </Button>
            <Button badge vertical onPress={this.temperatura}>
              <Badge><Text>2</Text></Badge>
              <Icon name="thermometer" />
              <Text>Temp</Text>
            </Button>
            <Button  badge vertical onPress={this.red}>
              <Badge ><Text>1</Text></Badge>
              <Icon  name="navigate" />
              <Text>Red</Text>
            </Button>
            <Button  vertical onPress={this.usuario}>
              <Icon name="person" />
              <Text>Usuario</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}