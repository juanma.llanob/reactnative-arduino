import React, { Component } from "react";
import {StyleSheet} from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Item, Input, Label,Icon, Picker, Form } from "native-base";



 export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
          selected: "key0"
        };
      }
      onValueChange(value: string) {
        this.setState({
          selected: value
        });
      }
    menu =() =>{
        this.props.navigation.navigate('Menu')
      }  
  render() {
    return (
      <Container  >
        <Content padder contentContainerStyle={styles.contenido} >
        
            
          
          <Card>
            <CardItem header >
              <Text style={styles.tittle} >Login </Text>
            </CardItem>
            <CardItem >
              <Body>
                <Item floatingLabel style={styles.input}>
                  <Label>Nombre</Label>
                  <Input />
                </Item>
                <Item floatingLabel style={styles.input}>
                  <Label>Apellido</Label>
                  <Input />
                </Item>
                <Item floatingLabel style={styles.input}>
                  <Label>Ciudad</Label>
                  <Input />
                </Item>
                <Item style={styles.input}>
                <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    headerStyle={{ backgroundColor: "green" }}
                    headerBackButtonTextStyle={{ color: "#fff" }}
                    headerTitleStyle={{ color: "#fff" }}
                    selectedValue={this.state.selected}
                    onValueChange={this.onValueChange.bind(this)}
                    style={styles.pick}>
                    <Picker.Item label="Peru" value="key0" />
                    <Picker.Item label="Agentina" value="key1" />
                    <Picker.Item label="Chile" value="key2" />
                    <Picker.Item label="Colombia" value="key3" />
                    <Picker.Item label="Brasil" value="key4" />
                </Picker>
                </Item>
                <Item floatingLabel style={styles.input}>
                  <Label>Correo</Label>
                  <Input />
                </Item>
                <Item floatingLabel style={styles.input}>
                  <Label>Password</Label>
                  <Input />
                </Item>
                <Item floatingLabel style={styles.input}>
                  <Label>Confirmar Password</Label>
                  <Input />
                </Item>
              </Body>
            </CardItem>
            <CardItem footer >
            <Button primary style={styles.btn_primary} onPress={this.menu}
            ><Text>REGISTRAR </Text></Button>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

import fondo from '../../../assets/fondo.jpg'
import { registerRootComponent } from "expo";
const styles = StyleSheet.create({

  contenido: {
    flex : 1,
    justifyContent : 'center'
  },
  tittle : {
    fontSize : 30,
    color : 'green',
    textAlign : 'center',
    width : '100%'
  },
  btn_primary : {
    backgroundColor: 'green',
    textAlign: 'center',
    justifyContent : 'center',
    width : '100%'
  },
  input:{
    marginTop : 20
  },
  pick : {
    marginLeft: -10,
    width:'67%',
    paddingRight : 0

  }
})



