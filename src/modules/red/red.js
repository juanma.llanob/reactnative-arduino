import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';
export default class Red extends Component {
    menu =() =>{
        this.props.navigation.navigate('Menu')
      }
      humedad =() =>{
        this.props.navigation.navigate('Humedad')
      }
      temperatura =() =>{
        this.props.navigation.navigate('Temperatura')
      }
       red=() =>{
        this.props.navigation.navigate('Red')
      }
      usuario=() =>{
        this.props.navigation.navigate('Usuario')
      }



      render() {
        return (
          <Container>
            <Content>
                <ListItem icon>
                    <Left>
                    <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="airplane" />
                    </Button>
                    </Left>
                    <Body>
                    <Text>Airplane Mode</Text>
                    </Body>
                    <Right>
                    <Switch value={false} />
                    </Right>
                </ListItem>
                <ListItem icon>
                    <Left>
                    <Button style={{ backgroundColor: "#007AFF" }}>
                        <Icon active name="wifi" />
                    </Button>
                    </Left>
                    <Body>
                    <Text>Wi-Fi</Text>
                    </Body>
                    <Right>
                    <Text>GeekyAnts</Text>
                    <Icon active name="arrow-forward" />
                    </Right>
                </ListItem>
                <ListItem icon>
                    <Left>
                    <Button style={{ backgroundColor: "#007AFF" }}>
                        <Icon active name="bluetooth" />
                    </Button>
                    </Left>
                    <Body>
                    <Text>Bluetooth</Text>
                    </Body>
                    <Right>
                    <Text>On</Text>
                    <Icon active name="arrow-forward" />
                    </Right>
                </ListItem>
            </Content>
            <Footer>
              <FooterTab>
                <Button badge vertical onPress={this.humedad}>
                  <Badge><Text>2</Text></Badge>
                  <Icon name="cloud" />
                  <Text>Hume</Text>
                </Button>
                <Button badge vertical onPress={this.temperatura}>
                  <Badge><Text>2</Text></Badge>
                  <Icon name="thermometer" />
                  <Text>Temp</Text>
                </Button>
                <Button active badge vertical onPress={this.red}>
                  <Badge ><Text>1</Text></Badge>
                  <Icon  name="navigate" />
                  <Text>Red</Text>
                </Button>
                <Button  vertical onPress={this.usuario}>
                  <Icon name="person" />
                  <Text>Usuario</Text>
                </Button>
              </FooterTab>
            </Footer>
          </Container>
        );
      }
}