import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge,List, ListItem, Separator } from 'native-base';
export default class Humedad extends Component {
    menu =() =>{
        this.props.navigation.navigate('Menu')
      }
      humedad =() =>{
        this.props.navigation.navigate('Humedad')
      }
      temperatura =() =>{
        this.props.navigation.navigate('Temperatura')
      }
       red=() =>{
        this.props.navigation.navigate('Red')
      }
      usuario=() =>{
        this.props.navigation.navigate('Usuario')
      }



      render() {
        return (
          <Container>
            <Content>
          <Separator bordered>
            <Text>Jardin Eden</Text>
          </Separator>
          <ListItem>
            <Text>Humedad Actual </Text>
            <Text style={styles.text_right}> 20º </Text>
          </ListItem>
          <ListItem last>
          <Text>Humedad mas Alta </Text>
            <Text style={styles.text_right}> 33º </Text>
          </ListItem>
          <Separator bordered>
            <Text>Historial </Text>
          </Separator>
          <ListItem>
            <Text>Humedad mas Lunes</Text>
            <Text style={styles.text_right}>04-11-19 33º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Domingo</Text>
            <Text style={styles.text_right}>03-11-19 31º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Sabado</Text>
            <Text style={styles.text_right}>02-11-19 38º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Viernes</Text>
            <Text style={styles.text_right}>01-11-19 39º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Jueves</Text>
            <Text style={styles.text_right}>31-10-19 37º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Miercoles</Text>
            <Text style={styles.text_right}>30-10-19 36º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Martes</Text>
            <Text style={styles.text_right}>29-10-19 35º</Text>
          </ListItem>
          <ListItem>
            <Text>Humedad mas Lunes</Text>
            <Text style={styles.text_right}>28-10-19 38º</Text>
          </ListItem>
          
        </Content>
            <Footer>
              <FooterTab>
                <Button badge vertical onPress={this.humedad}>
                  <Badge><Text>2</Text></Badge>
                  <Icon name="cloud" />
                  <Text>Hume</Text>
                </Button>
                <Button active badge vertical onPress={this.temperatura}>
                  <Badge><Text>2</Text></Badge>
                  <Icon name="thermometer" />
                  <Text>Temp</Text>
                </Button>
                <Button  badge vertical onPress={this.red}>
                  <Badge ><Text>1</Text></Badge>
                  <Icon  name="navigate" />
                  <Text>Red</Text>
                </Button>
                <Button  vertical onPress={this.usuario}>
                  <Icon name="person" />
                  <Text>Usuario</Text>
                </Button>
              </FooterTab>
            </Footer>
          </Container>
        );
      }
}

const styles = StyleSheet.create({
    text_right : {
        marginLeft:200,
        fontSize: 14,
        color : 'silver'
        
    }
})